from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from pydantic import BaseModel, AnyUrl
import hashlib
import json

app = FastAPI()

try:
    with open("data.json", "r") as f:
        data = json.load(f)
except:
    file = open("data.json", "x")
    data = {}

class ShortenUrl(BaseModel):
    url: AnyUrl

@app.get("/{shorten_url}")
def move(shorten_url) -> RedirectResponse:
    url = data[shorten_url]
    return RedirectResponse(url=f"{url}")

@app.get("/")
def root():
    return "Hello. This is a url shortener service. Try /docs."

@app.post("/create/")
def create(url: ShortenUrl):
    url = url.url
    data[f"{hashlib.shake_256(url.encode()).hexdigest(3)}"] = f"{url}"
    print(data)
    return f"http://127.0.0.1:8000/{hashlib.shake_256(url.encode()).hexdigest(3)}"

@app.on_event("shutdown")
def shutdown_event():
    with open("data.json", "w") as f:
        json.dump(data, f)
